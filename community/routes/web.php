<?php
use App\Models\CommunityLink;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();
Auth::routes(['verify' => 'true']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'verified'], function () {

// Rutas a verificar


});
//para mostrar todos los links que llamará al método index mediante GET
Route::get('community', [App\Http\Controllers\CommunityLinkController::class, 'index'])
->middleware('auth');
//para crear un link que llamará al método store del controlador mediante POST
Route::post('community', [App\Http\Controllers\CommunityLinkController::class, 'store'])
->middleware('auth');

//será el método index el encargado de hacer el filtrado dependiendo 
//del parámetro channel
Route::get('community/{channel:slug}', [App\Http\Controllers\CommunityLinkController::class, 'index']);










/*
Route::get('ejemplo', function (Request $request){
    $name = $request->fullUrl();
    return dd($name);
});*/
/*
Route::get('/home', function() {

    return response('Error', 404);
    //->header('Content-type', 'text/plain');

});*/